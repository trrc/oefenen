package nl.deventer.oefenen.classes;

import nl.deventer.oefenen.interfaces.Interface1;
import nl.deventer.oefenen.interfaces.Interface2;

public class ClassMetAbstracteMethoden implements Interface1, Interface2{

	@Override
	public void doeHet() {
		System.out.println("Ik doe het");
	}
	


}